import logging, unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
#https://qxf2.com/blog/sauce-labs-part/ 
 
class Selenium2OnSauce(unittest.TestCase):
 
    def setUp(self):
        self.driver = webdriver.Firefox()
 
    def test_search_in_python_org(self):
        #Go to the URL 
        self.driver.get("http://www.python.org")
 
        #Assert that the title is correct
        self.assertIn("Python", self.driver.title)
 
        #Identify the xpath and send the string you want
        elem = self.driver.find_element_by_xpath("//input[@id='id-search-field']")
        print "About to search for the string BeautifulSoup on python.org"
        elem.send_keys("BeautifulSoup")
        elem.send_keys(Keys.RETURN)
 
 
    def tearDown(self):
        self.driver.quit()
 
if __name__ == '__main__':
    unittest.main()		
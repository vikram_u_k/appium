from optparse import OptionParser
# https://qxf2.com/blog/running-selenium-automation-using-sauce-labs-part-2/
class Selenium2OnSauce(unittest.TestCase):
    "Example class written to run Selenium tests on Sauce Labs"
    def __init__(self,browser,browser_version,platform):
        "Constructor: Accepts the browser, browser version and OS(platform) as parameters"
        self.browser = browser
        self.browser_version = browser_version
        self.platform = platform
 
    def setUp(self):
        "Setup for this test involves spinning up the right virtual machine on Sauce Labs"
        if self.browser.lower() == 'ff' or self.browser.lower() == 'firefox':
            desired_capabilities = webdriver.DesiredCapabilities.FIREFOX
            desired_capabilities['version'] = self.browser_version
            desired_capabilities['platform'] = self.platform
        elif self.browser.lower() == 'ie':
            desired_capabilities = webdriver.DesiredCapabilities.INTERNETEXPLORER
            desired_capabilities['version'] = self.browser_version
            desired_capabilities['platform'] = self.platform
        elif self.browser.lower() == 'chrome':
            desired_capabilities = webdriver.DesiredCapabilities.CHROME
            desired_capabilities['version'] = self.browser_version
            desired_capabilities['platform'] = self.platform
 
        desired_capabilities['name'] = 'Testing Script in different Browser, Version and Platform'
        self.driver = webdriver.Remote(
            desired_capabilities=desired_capabilities,
            command_executor="http://$USERNAME:$ACCESS_KEY@ondemand.saucelabs.com:80/wd/hub"
        )
        self.driver.implicitly_wait(30)


#---START OF SCRIPT
if __name__ == '__main__':
    #Lets accept some command line options from the user
    #We have chosen to use the Python module optparse 
    usage = "usage: %prog -b <browser> -v <browser version number> -p <platform>\nE.g.1: %prog -b ie -v 8 -p \"Windows 7\"\nE.g.2: %prog -b ff -v 26 -p \"Windows 8\"\n---"
    parser = OptionParser(usage=usage)
    parser.add_option("-b","--browser",dest="browser",help="The name of the browser: ie, firefox, chrome",default="firefox")
    parser.add_option("-v","--version",dest="browser_version",help="The version of the browser: a whole number",default=None)
    parser.add_option("-p","--platform",dest="platform",help="The operating system: Windows 7, Linux",default="Windows 7")
    (options,args) = parser.parse_args()